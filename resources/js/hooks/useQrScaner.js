import QrScanner from 'qr-scanner';
import { onBeforeUnmount, onMounted, ref, reactive } from 'vue';

QrScanner.WORKER_PATH = 'js/qr-scanner-worker.min.js'

const useQrScanner = () => {
    const videoScanner = ref(null);
    const qrScanner = ref(null);
    const state = reactive({
        loading: true,
        qrValue: null,
        listCameras: [],
        activeCamera: null,
        isCamera: false,
        scanning: false,
        isFlash: false,
        isFlashOn: false,
        error: '',
    });

    const setQrValue = (res) => {
        state.qrValue = res
    }

    const initQr = async () => {
        state.loading = true;
        state.error = '';
        try {
            state.isCamera = await QrScanner.hasCamera();
            if (videoScanner.value && state.isCamera) {
                state.listCameras = await QrScanner.listCameras(true);
                state.activeCamera = state.listCameras?.[0]?.id;
                state.isFlash = qrScanner.hasFlash();
                state.isFlashOn = qrScanner.value.isFlashOn();
                qrScanner.value = new QrScanner(videoScanner.value, setQrValue);
                return
            }
            state.error = 'Sorry we were unable to access the camera.';
        } catch (e) {
            state.error = e.messages;
        } finally {
            state.loading = false;
        }
    }

    const toggleScanning = async () => {
        state.scanning = !state.scanning;
        try {
            if (state.scanning) {
                await qrScanner.value.start();
                return
            }
            qrScanner.value.stop();
        } catch (e) {
            state.error = e.messages;
        }
    }

    const toggleFlash = async () => {
        try {
            if (!state.isFlashOn) {
               await qrScanner.value.turnFlashOn();
               state.isFlashOn = true;
               return
            }
            await qrScanner.value.turnFlashOff();
            state.isFlashOn = false;
        } catch (e) {
            state.error = e.messages;
        }
    }


    const setCamera = (e) => {
        const value = e.target.value;
        state.activeCamera = value;
        qrScanner.setCamera(value);
    }

    onMounted(initQr);

    onBeforeUnmount(() => {
        if (!qrScanner.value) return;
        qrScanner.value.destroy()
    });

    return {
        qrScanner,
        videoScanner,
        state,
        setCamera,
        toggleScanning,
        toggleFlash
    }
}

export default useQrScanner;
