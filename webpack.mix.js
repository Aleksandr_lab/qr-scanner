const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/main.js', 'public/js')
    .vue()
    .postCss('resources/css/app.css', 'public/css');

mix.copy('node_modules/qr-scanner/qr-scanner-worker.min.js', 'public/js')

if (!mix.inProduction()) {
    mix.browserSync('127.0.0.1:8000');
} else {
    mix.version();
}
